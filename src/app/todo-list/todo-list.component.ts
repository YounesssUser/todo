import { Component, OnInit } from '@angular/core';
import { todos } from '../todos';
import {Renderer} from '@angular/core';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {

  list = todos;
  classes = "";
  note:string = "";

  constructor(private render:Renderer) { }

  ngOnInit() {
  }

  delete(id: number){
    this.list = this.list.filter(function(value, index, arr){ return value.id !== id;});
  }

  AddNote(){
    if(this.note.length > 0){
      let sortedList = this.list.sort((a, b) => a.id - b.id);
      let lastId = sortedList[sortedList.length - 1].id + 1;
      this.list.push({id: lastId, description: this.note});
      this.note = "";
    }
  }
}
